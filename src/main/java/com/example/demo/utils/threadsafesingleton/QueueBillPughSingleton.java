package com.example.demo.utils.threadsafesingleton;

import com.dz7.common.utils.DataLoggerUtils;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueBillPughSingleton {
    private static Queue queue;
    private static class SingletonHelper{
        private static final QueueBillPughSingleton INSTANCE = new QueueBillPughSingleton();
    }
    private QueueBillPughSingleton(){
            queue = new LinkedBlockingQueue<String>();
            DataLoggerUtils.setQueue(queue);
            System.out.println("Load constructor and creating queue");
            if(queue == null){
                System.out.println("null queue");
            }
    }


    public static QueueBillPughSingleton getInstance(){
        return SingletonHelper.INSTANCE;
    }

}
