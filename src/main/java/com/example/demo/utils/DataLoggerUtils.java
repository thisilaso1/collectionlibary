package com.example.demo.utils;


import com.example.demo.exceptions.QueueNullPointerException;

import java.util.Queue;

public final class DataLoggerUtils {
    private static Queue innerQueue;
    public static void setQueue(Queue queueContext){
        innerQueue = queueContext;
        System.out.println("data set queue");
        System.out.println(innerQueue == null);
    }
    public static Queue queue() throws QueueNullPointerException {
        if(innerQueue == null){
            throw new QueueNullPointerException();
        }
        return innerQueue;
    }
}
