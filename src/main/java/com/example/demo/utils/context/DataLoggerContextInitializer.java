package com.example.demo.utils.context;

import com.example.demo.utils.threadsafesingleton.QueueBillPughSingleton;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DataLoggerContextInitializer {

   @Bean
   public void init(){
      QueueBillPughSingleton.getInstance();
   }

}
