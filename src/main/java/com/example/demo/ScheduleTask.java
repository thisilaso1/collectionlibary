package com.example.demo;


import com.example.demo.exceptions.QueueNullPointerException;
import com.example.demo.utils.DataLoggerUtils;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.util.Queue;

@Component
public class ScheduleTask {

    @Scheduled(fixedRate = 5000)
    public void scheduleTaskSaveQueue() {
        try {
            Queue mess =  DataLoggerUtils.queue();
            System.out.println("queue: " + mess.toString());


        } catch (QueueNullPointerException e) {
            e.printStackTrace();
        }
    }
}
