package com.example.demo.constants;

public enum ExceptionHandel {
    QueueEmptyException("Queue is Null Pointer");
    private String value;

    ExceptionHandel(String s) {
        this.value = s;
    }
     public String getMessage(){
        return value;
     }
}
