package com.example.demo.exceptions;


import com.example.demo.constants.ExceptionHandel;

public class QueueNullPointerException extends Exception {
    public QueueNullPointerException(){
        super(ExceptionHandel.QueueEmptyException.getMessage());
    }
}
