package com.example.demo;


import com.example.demo.exceptions.QueueNullPointerException;
import com.example.demo.utils.DataLoggerUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

@Controller
@CrossOrigin("*")
@RequestMapping("/api")
public class ControllerTest {


    @PostMapping("/send")
    public ResponseEntity getData(@RequestBody  Object value)  {
        System.out.println("value: " + value);
        if(value == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        try{
            Queue mess =  DataLoggerUtils.queue();
            System.out.println("queue: " + mess.toString());
            mess.add(value);
        }
        catch (QueueNullPointerException e) {
            e.printStackTrace();
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
